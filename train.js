const fs = require('fs');
const msgpack = require("msgpack-lite");
const readline = require('readline');


async function processLineByLine() {
    let model = new Map();
    const fileStream = fs.createReadStream('horoscopes.txt');

    const rl = readline.createInterface({
        input: fileStream,
        crlfDelay: Infinity
    });
    // Note: we use the crlfDelay option to recognize all instances of CR LF
    // ('\r\n') in input.txt as a single line break.

    for await (const line of rl) {
        // Each line in input.txt will be successively available here as `line`.
        let words = line.trim().split(' ')
        if(words.length < 3)
            continue;
        for (let i = 0; i < words.length - 3;i++) {
            let first = words[i].toLowerCase();
            let second = words[i+1].toLowerCase();
            let result = words[i+2];
            let key = `${first} ${second}`;
            if(model.has(key))
                model.get(key).push(result);
            else
                model.set(key,[result]);
        }
    }
    return model;
}

(async function main() {
    var model = await processLineByLine();
    fs.writeFile('model.msgpack',msgpack.encode(Array.from(model.entries())),  (err) => {
        if (err) throw err;
        console.log('The file has been saved!');
    });
})();
