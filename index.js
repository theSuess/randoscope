let model = null;
const minLength = 20;
const maxLength = 60;

fetch(new Request('model.msgpack'))
    .then((response) => {
        if(!response.ok) {
            throw new Error('HTTP error, status = ' + response.status);
        }
        return response.arrayBuffer();
    })
    .then((blob) => {
        model = new Map(msgpack.decode(new Uint8Array(blob)));
    });


document.querySelector('#action').onclick=() => {
    if(model == null) {
        alert('Our fortunes are still warming up...');
        return;
    }

    let items = Array.from(model);
    let words = [];
    Array.prototype.push.apply(words,items[Math.floor(Math.random()*items.length)][0].split(" "));
    let stopReached = false;
    while((words.length < maxLength) && (words.length < minLength || !stopReached)) {
        let idx = words.length;
        let p1 = words[idx-1];
        let p2 = words[idx-2];
        let key = `${p2.toLowerCase()} ${p1.toLowerCase()}`;
        if(!model.has(key))
            key = items[Math.floor(Math.random()*items.length)][0]
        let predictions = model.get(key);
        let next = predictions[Math.floor(Math.random() * predictions.length)];
        words.push(next);
        stopReached = next.indexOf(".") != -1
    }
    document.querySelector('#fortune').textContent = words.join(' ');
}
